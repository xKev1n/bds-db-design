-- SQLINES DEMO *** rated by MySQL Workbench
-- SQLINES DEMO *** 09 2021
-- SQLINES DEMO ***    Version: 1.0
-- SQLINES DEMO *** orward Engineering

/* SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0; */
/* SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0; */
/* SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION'; */

-- SQLINES DEMO *** ------------------------------------
-- Schema lib_db
-- SQLINES DEMO *** ------------------------------------

-- SQLINES DEMO *** ------------------------------------
-- Schema lib_db
-- SQLINES DEMO *** ------------------------------------
CREATE SCHEMA IF NOT EXISTS lib_db AUTHORIZATION postgres ;
COMMENT ON SCHEMA lib_db
    IS 'BDS Project';

GRANT ALL ON SCHEMA lib_db TO PUBLIC;

GRANT ALL ON SCHEMA lib_db TO postgres;

-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** ary`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Salary (
  idSalary INT NOT NULL,
  salary REAL UNIQUE NOT NULL,
  details VARCHAR(45) NULL,
  PRIMARY KEY (idSalary))
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** loyeeRole`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.EmployeeRole (
  idRole INT NOT NULL,
  role VARCHAR(45) UNIQUE NOT NULL,
  PRIMARY KEY (idRole))
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** loyee`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Employee (
  idEmployee INT NOT NULL,
  FirstName VARCHAR(45) NOT NULL,
  Surname VARCHAR(45) NOT NULL,
  Birthdate DATE NOT NULL,
  Title VARCHAR(45) NULL,
  MiddleName VARCHAR(45) NULL,
  Sex VARCHAR(45) NULL,
  Salary_idSalary INT NOT NULL,
  EmployeeRole_idRole INT NOT NULL,
  PRIMARY KEY (idEmployee),
  CONSTRAINT fk_Employee_Salary1
    FOREIGN KEY (Salary_idSalary)
    REFERENCES lib_db.Salary (idSalary)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Employee_EmployeeRole1
    FOREIGN KEY (EmployeeRole_idRole)
    REFERENCES lib_db.EmployeeRole (idRole)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Employee_Salary1_idx ON lib_db.Employee (Salary_idSalary ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Employee_EmployeeRole1_idx ON lib_db.Employee (EmployeeRole_idRole ASC) ;


-- SQLINES DEMO *** ------------------------------------
-- Table `lib_db`.`ISBN`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.ISBN (
  ISBN VARCHAR(45) UNIQUE NOT NULL,
  idISBN INT NOT NULL,
  PRIMARY KEY (idISBN))
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** lisher`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Publisher (
  idPublisher INT NOT NULL,
  name VARCHAR(45) UNIQUE NOT NULL,
  PRIMARY KEY (idPublisher))
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** nsaction`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Transaction (
  idTransaction INT NOT NULL,
  type VARCHAR(45) NOT NULL,
  PRIMARY KEY (idTransaction))
;


-- SQLINES DEMO *** ------------------------------------
-- Table `lib_db`.`Book`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Book (
  idBook INT NOT NULL,
  state VARCHAR(45) NOT NULL,
  n_o_pages INT NOT NULL,
  ISBN_idISBN INT NOT NULL,
  which_edition INT NOT NULL,
  published DATE NOT NULL,
  name VARCHAR(45) NOT NULL,
  Publisher_idPublisher INT NOT NULL,
  Transaction_idTransaction INT NULL,
  PRIMARY KEY (idBook),
  CONSTRAINT fk_Book_ISBN1
    FOREIGN KEY (ISBN_idISBN)
    REFERENCES lib_db.ISBN (idISBN)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Book_Publisher1
    FOREIGN KEY (Publisher_idPublisher)
    REFERENCES lib_db.Publisher (idPublisher)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Book_Transaction1
    FOREIGN KEY (Transaction_idTransaction)
    REFERENCES lib_db.Transaction (idTransaction)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Book_ISBN1_idx ON lib_db.Book (ISBN_ISBN ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Book_Publisher1_idx ON lib_db.Book (Publisher_idPublisher ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Book_Transaction1_idx ON lib_db.Book (Transaction_idTransaction ASC) ;





-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** tomer`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Customer (
  idCustomer INT NOT NULL,
  FirstName VARCHAR(45) NOT NULL,
  Surname VARCHAR(45) NOT NULL,
  Birthdate DATE NOT NULL,
  MiddleName VARCHAR(45) NULL,
  Sex VARCHAR(45) NULL,
  Title VARCHAR(45) NULL,
  PRIMARY KEY (idCustomer));




-- SQLINES DEMO *** ------------------------------------
-- Table `lib_db`.`Rent`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Rent (
  Transaction_idTransaction INT NOT NULL,
  end_date DATE NOT NULL,
  start_date DATE NOT NULL,
  PRIMARY KEY (Transaction_idTransaction),
  CONSTRAINT fk_Rent_Transaction1
    FOREIGN KEY (Transaction_idTransaction)
    REFERENCES lib_db.Transaction (idTransaction)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Rent_Transaction1_idx ON lib_db.Rent (Transaction_idTransaction ASC) ;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** ress`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Address (
  idAddress INT NOT NULL,
  city VARCHAR(45) NOT NULL,
  h_number VARCHAR(45) NOT NULL,
  ZIP_code VARCHAR(45) NOT NULL,
  street VARCHAR(45) NULL,
  country VARCHAR(45) NOT NULL,
  PRIMARY KEY (idAddress))
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** tact`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Contact (
  idContact INT NOT NULL,
  type VARCHAR(45) NOT NULL,
  details VARCHAR(45) UNIQUE NOT NULL,
  PRIMARY KEY (idContact))
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** loyee_has_contact`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.employee_has_contact (
  Employee_idEmployee INT NOT NULL,
  Contact_idContact INT NOT NULL,
  CONSTRAINT fk_employee_has_contact_Employee1
    FOREIGN KEY (Employee_idEmployee)
    REFERENCES lib_db.Employee (idEmployee)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_employee_has_contact_Contact1
    FOREIGN KEY (Contact_idContact)
    REFERENCES lib_db.Contact (idContact)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_employee_has_contact_Employee1_idx ON lib_db.employee_has_contact (Employee_idEmployee ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_employee_has_contact_Contact1_idx ON lib_db.employee_has_contact (Contact_idContact ASC) ;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** tomer_has_contact`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.customer_has_contact (
  Customer_idCustomer INT NOT NULL,
  Contact_idContact INT NOT NULL,
  CONSTRAINT fk_customer_has_contact_Customer1
    FOREIGN KEY (Customer_idCustomer)
    REFERENCES lib_db.Customer (idCustomer)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_customer_has_contact_Contact1
    FOREIGN KEY (Contact_idContact)
    REFERENCES lib_db.Contact (idContact)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_customer_has_contact_Customer1_idx ON lib_db.customer_has_contact (Customer_idCustomer ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_customer_has_contact_Contact1_idx ON lib_db.customer_has_contact (Contact_idContact ASC) ;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** hor`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Author (
  idAuthor INT NOT NULL,
  FirstName VARCHAR(45) NOT NULL,
  Surname VARCHAR(45) NOT NULL,
  Title VARCHAR(45) NULL,
  MiddleName VARCHAR(45) NULL,
  Sex VARCHAR(45) NULL,
  PRIMARY KEY (idAuthor))
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** k_has_author`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.book_has_author (
  Book_idBook INT NOT NULL,
  Author_idAuthor INT NOT NULL,
  CONSTRAINT fk_book_has_author_Books1
    FOREIGN KEY (Book_idBook)
    REFERENCES lib_db.Book (idBook)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_book_has_author_Author1
    FOREIGN KEY (Author_idAuthor)
    REFERENCES lib_db.Author (idAuthor)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_book_has_author_Author1_idx ON lib_db.book_has_author (Author_idAuthor ASC) ;





-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** tomer_has_transaction`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.customer_has_transaction (
  Customer_idCustomer INT NOT NULL,
  Transaction_idTransaction INT NOT NULL,
  CONSTRAINT fk_customer_has_transaction_Customer1
    FOREIGN KEY (Customer_idCustomer)
    REFERENCES lib_db.Customer (idCustomer)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_customer_has_transaction_Transaction1
    FOREIGN KEY (Transaction_idTransaction)
    REFERENCES lib_db.Transaction (idTransaction)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_customer_has_transaction_Customer1_idx ON lib_db.customer_has_transaction (Customer_idCustomer ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_customer_has_transaction_Transaction1_idx ON lib_db.customer_has_transaction (Transaction_idTransaction ASC) ;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** chase`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Purchase (
  Transaction_idTransaction INT NOT NULL,
  price REAL NOT NULL,
  PRIMARY KEY (Transaction_idTransaction),
  CONSTRAINT fk_Purchase_Transaction1
    FOREIGN KEY (Transaction_idTransaction)
    REFERENCES lib_db.Transaction (idTransaction)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Purchase_Transaction1_idx ON lib_db.Purchase (Transaction_idTransaction ASC) ;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** lisher_has_contact`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.publisher_has_contact (
  Publisher_idPublisher INT NOT NULL,
  Contact_idContact INT NOT NULL,
  CONSTRAINT fk_publisher_has_contact_Publisher1
    FOREIGN KEY (Publisher_idPublisher)
    REFERENCES lib_db.Publisher (idPublisher)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_publisher_has_contact_Contact1
    FOREIGN KEY (Contact_idContact)
    REFERENCES lib_db.Contact (idContact)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_publisher_has_contact_Publisher1_idx ON lib_db.publisher_has_contact (Publisher_idPublisher ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_publisher_has_contact_Contact1_idx ON lib_db.publisher_has_contact (Contact_idContact ASC) ;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** re`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Genre (
  idGenre INT NOT NULL,
  genre VARCHAR(45) UNIQUE NOT NULL,
  PRIMARY KEY (idGenre))
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** loyee_has_Address`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Employee_has_Address (
  Employee_idEmployee INT NOT NULL,
  Address_idAddress INT NOT NULL,
  addressType VARCHAR(45) NOT NULL,
  PRIMARY KEY (Employee_idEmployee, Address_idAddress),
  CONSTRAINT fk_Employee_has_Address_Employee1
    FOREIGN KEY (Employee_idEmployee)
    REFERENCES lib_db.Employee (idEmployee)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Employee_has_Address_Address1
    FOREIGN KEY (Address_idAddress)
    REFERENCES lib_db.Address (idAddress)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Employee_has_Address_Address1_idx ON lib_db.Employee_has_Address (Address_idAddress ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Employee_has_Address_Employee1_idx ON lib_db.Employee_has_Address (Employee_idEmployee ASC) ;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** tomer_has_Address`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Customer_has_Address (
  Customer_idCustomer INT NOT NULL,
  Address_idAddress INT NOT NULL,
  addressType VARCHAR(45) NOT NULL,
  PRIMARY KEY (Customer_idCustomer, Address_idAddress),
  CONSTRAINT fk_Customer_has_Address_Customer1
    FOREIGN KEY (Customer_idCustomer)
    REFERENCES lib_db.Customer (idCustomer)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Customer_has_Address_Address1
    FOREIGN KEY (Address_idAddress)
    REFERENCES lib_db.Address (idAddress)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Customer_has_Address_Address1_idx ON lib_db.Customer_has_Address (Address_idAddress ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Customer_has_Address_Customer1_idx ON lib_db.Customer_has_Address (Customer_idCustomer ASC) ;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** ress_has_Publisher`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Address_has_Publisher (
  Address_idAddress INT NOT NULL,
  Publisher_idPublisher INT NOT NULL,
  addressType VARCHAR(45) NOT NULL,
  PRIMARY KEY (Address_idAddress, Publisher_idPublisher),
  CONSTRAINT fk_Address_has_Publisher_Address1
    FOREIGN KEY (Address_idAddress)
    REFERENCES lib_db.Address (idAddress)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Address_has_Publisher_Publisher1
    FOREIGN KEY (Publisher_idPublisher)
    REFERENCES lib_db.Publisher (idPublisher)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Address_has_Publisher_Publisher1_idx ON lib_db.Address_has_Publisher (Publisher_idPublisher ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Address_has_Publisher_Address1_idx ON lib_db.Address_has_Publisher (Address_idAddress ASC) ;





-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** re_has_Book`
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS lib_db.Genre_has_Book (
  Genre_idGenre INT NOT NULL,
  Book_idBook INT NOT NULL,
  PRIMARY KEY (Genre_idGenre, Book_idBook),
  CONSTRAINT fk_Genre_has_Book_Genre1
    FOREIGN KEY (Genre_idGenre)
    REFERENCES lib_db.Genre (idGenre)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Genre_has_Book_Book1
    FOREIGN KEY (Book_idBook)
    REFERENCES lib_db.Book (idBook)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Genre_has_Book_Book1_idx ON lib_db.Genre_has_Book (Book_idBook ASC) ;

-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE INDEX fk_Genre_has_Book_Genre1_idx ON lib_db.Genre_has_Book (Genre_idGenre ASC) ;


/* SET SQL_MODE=@OLD_SQL_MODE; */
/* SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS; */
/* SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS; */

